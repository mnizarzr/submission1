package tech.mnizarzr.lmdb.utils

import com.beust.klaxon.FieldRenamer

val renamer = object : FieldRenamer {
    override fun toJson(fieldName: String) = FieldRenamer.camelToUnderscores(fieldName)
    override fun fromJson(fieldName: String) = FieldRenamer.underscoreToCamel(fieldName)
}
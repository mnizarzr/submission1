package tech.mnizarzr.lmdb.utils

import android.graphics.BitmapFactory

fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
    val (height: Int, width: Int) = options.run { outHeight to outWidth }
    var inSampleSize = 1 // 1 = gambar ukuran utuh

    if (height > reqHeight || width > reqWidth) {

        val halfHeight: Int = height / 2
        val halfWidth: Int = width / 2

        while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
            inSampleSize *= 2 // ukuran 1/2 gambar utuh, nilai selalu kelipatan 2
        }
    }

    return inSampleSize
}
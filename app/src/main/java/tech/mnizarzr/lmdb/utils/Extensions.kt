package tech.mnizarzr.lmdb.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.widget.TextView
import androidx.core.view.doOnPreDraw

/**
 * Dear reviewer,
 * plis jangan turunkan bintang saya
 * karena ada kode yang tidak saya gunakan
 * biar saya ingat pernah nulis ini
 * siapa tau suatu saat berguna 😅
 */
fun Context.createBitmapFromAsset(fileName: String): Bitmap? {
    this.assets.open(fileName).use { inputStream ->
        return BitmapFactory.Options().run {
            inJustDecodeBounds = true
            BitmapFactory.decodeStream(
                inputStream,
                null,
                this
            ) // Decode bitmap tanpa alokasi memory untuk piksel bitmap
            inputStream.reset()
            inSampleSize = calculateInSampleSize(this, 120, 170)

            inJustDecodeBounds = false
            BitmapFactory.decodeStream(inputStream, null, this) // Decode sebenarnya
        }
    }
}

fun TextView.setMaxLinesToEllipsize() = doOnPreDraw {
    val numberOfCompletelyVisibleLines = (measuredHeight - paddingTop - paddingBottom) / lineHeight
    maxLines = numberOfCompletelyVisibleLines
}
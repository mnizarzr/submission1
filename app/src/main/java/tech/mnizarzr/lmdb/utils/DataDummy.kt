package tech.mnizarzr.lmdb.utils

import android.content.Context
import android.util.Log
import com.beust.klaxon.Klaxon
import tech.mnizarzr.lmdb.entity.ItemMovieTv
import java.io.IOException

object DataDummy {

    private val TAG = this.javaClass.simpleName

    private val klaxon = Klaxon().fieldRenamer(renamer)

    enum class FileName {
        Movies,
        TvShows
    }

    fun parseJsonAsset(context: Context, fileName: FileName): List<ItemMovieTv> {
        return try {
            val assetFileName: String = when (fileName) {
                FileName.Movies -> "movies.json"
                FileName.TvShows -> "tvs.json"
            }
            context.assets.open(assetFileName).use {
                klaxon.parseArray(it) ?: emptyList() // lebih baik list kosong daripada null
            }
        } catch (e: IOException) {
            e.message?.let { m -> Log.d(TAG, m) }
            emptyList()
        }
    }
}
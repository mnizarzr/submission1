package tech.mnizarzr.lmdb.ui

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation
import tech.mnizarzr.lmdb.R
import tech.mnizarzr.lmdb.databinding.ItemMovieBinding
import tech.mnizarzr.lmdb.entity.ItemMovieTv
import tech.mnizarzr.lmdb.utils.setMaxLinesToEllipsize

class ItemMovieTvAdapter(val itemKind: String) :
    RecyclerView.Adapter<ItemMovieTvAdapter.ItemAdapter>() {

    private val listTvShows = ArrayList<ItemMovieTv>()

    fun setListMovieTvs(items: List<ItemMovieTv>?) {
        if (items == null) return
        this.listTvShows.clear()
        this.listTvShows.addAll(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter =
        ItemAdapter(ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ItemAdapter, position: Int) {
        holder.bind(listTvShows[position])
    }

    override fun getItemCount(): Int = listTvShows.size

    inner class ItemAdapter(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(itemMovieTv: ItemMovieTv) {
            with(binding) {

                val fileUri = "file:///android_asset/poster/" + itemKind + itemMovieTv.posterPath
                imgPoster.load(Uri.parse(fileUri)) {
                    transformations(RoundedCornersTransformation(8f))
                    crossfade(true)
                    placeholder(R.color.purple_200)
                }

                txtMovieTitle.text = itemMovieTv.title
                txtMovieOverview.apply {
                    text = itemMovieTv.overview
                    setMaxLinesToEllipsize()
                }
                txtMovieReleaseDate.text = itemMovieTv.releaseDate

            }
        }

    }
}
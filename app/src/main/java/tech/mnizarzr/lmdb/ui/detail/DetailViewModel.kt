package tech.mnizarzr.lmdb.ui.detail

import android.content.Context
import androidx.lifecycle.ViewModel
import tech.mnizarzr.lmdb.entity.ItemMovieTv
import tech.mnizarzr.lmdb.utils.DataDummy

class DetailViewModel : ViewModel() {

    private var itemId: Int? = null

    fun setSelectedItem(itemId: Int) {
        this.itemId = itemId
    }

    fun getItem(context: Context): ItemMovieTv {
        lateinit var item: ItemMovieTv
        val movieItems = DataDummy.parseJsonAsset(context, DataDummy.FileName.Movies)
        val tvItems = DataDummy.parseJsonAsset(context, DataDummy.FileName.TvShows)
        val items = movieItems + tvItems
        items.forEach { i -> if (i.id == this.itemId) item = i }
        return item
    }

}
package tech.mnizarzr.lmdb.ui.tvshow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import tech.mnizarzr.lmdb.databinding.FragmentTvShowBinding
import tech.mnizarzr.lmdb.ui.ItemMovieTvAdapter

class TvShowFragment : Fragment() {

    private lateinit var viewModel: TvShowViewModel
    private lateinit var adapter: ItemMovieTvAdapter
    private var _binding: FragmentTvShowBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTvShowBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity != null) {
            viewModel = ViewModelProvider(
                requireActivity(),
                ViewModelProvider.NewInstanceFactory()
            )[TvShowViewModel::class.java]
            viewModel.getTvShows(requireContext())

            adapter = ItemMovieTvAdapter("tvs")
            adapter.setListMovieTvs(viewModel.listTvShows)

            binding.rvTv.apply {
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
                adapter = this@TvShowFragment.adapter
                addItemDecoration(
                    DividerItemDecoration(
                        this.context,
                        DividerItemDecoration.VERTICAL
                    )
                )
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        private val TAG = TvShowFragment::class.java.simpleName

    }

}
package tech.mnizarzr.lmdb.ui.tvshow

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import tech.mnizarzr.lmdb.entity.ItemMovieTv
import tech.mnizarzr.lmdb.utils.DataDummy

class TvShowViewModel : ViewModel() {

    var listTvShows: List<ItemMovieTv> = emptyList()

    fun getTvShows(context: Context) {
        if (listTvShows.isEmpty()) {
            viewModelScope.launch(Dispatchers.IO) {
                listTvShows = DataDummy.parseJsonAsset(context, DataDummy.FileName.TvShows)
            }
        }
    }
}
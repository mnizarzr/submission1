package tech.mnizarzr.lmdb.ui.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import tech.mnizarzr.lmdb.databinding.ActivityDetailItemBinding

class DetailItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityDetailItemBinding = ActivityDetailItemBinding.inflate(layoutInflater)
        setContentView(activityDetailItemBinding.root)


    }

}
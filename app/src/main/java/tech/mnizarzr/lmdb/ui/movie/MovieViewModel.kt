package tech.mnizarzr.lmdb.ui.movie

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import tech.mnizarzr.lmdb.entity.ItemMovieTv
import tech.mnizarzr.lmdb.utils.DataDummy

class MovieViewModel : ViewModel() {

    fun getMovies(context: Context): List<ItemMovieTv> {

        val listMovies = ArrayList<ItemMovieTv>()

        if (listMovies.isEmpty())
            viewModelScope.launch(Dispatchers.IO) {
                val movies = DataDummy.parseJsonAsset(context, DataDummy.FileName.Movies)
                withContext(Dispatchers.Main) {
                    listMovies.addAll(movies)
                }
            }

        return listMovies
    }

}
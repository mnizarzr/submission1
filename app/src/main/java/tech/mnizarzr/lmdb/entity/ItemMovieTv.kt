package tech.mnizarzr.lmdb.entity

data class ItemMovieTv(
    var id: Int,
    var title: String,
    var backdropPath: String,
    var overview: String,
    var posterPath: String,
    var releaseDate: String
)
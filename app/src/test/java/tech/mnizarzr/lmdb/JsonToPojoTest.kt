package tech.mnizarzr.lmdb

import com.beust.klaxon.JsonReader
import com.beust.klaxon.Klaxon
import org.junit.Assert.assertEquals
import org.junit.Test
import tech.mnizarzr.lmdb.entity.ItemMovieTv
import tech.mnizarzr.lmdb.utils.renamer
import java.io.StringReader

class JsonToPojoTest {

    private val jsonFileName = "movies.json"

    @Test
    fun readFromJsonFileTest() {
        val movie = ClassLoader.getSystemResource(jsonFileName)?.readText()?.let {
            val klaxon = Klaxon().fieldRenamer(renamer)
            val result = arrayListOf<ItemMovieTv>()
            JsonReader(StringReader(it)).use { reader ->
                reader.beginArray {
                    while (reader.hasNext()) {
                        val item = klaxon.parse<ItemMovieTv>(reader)
                        if (item != null) {
                            result.add(item)
                        }
                    }
                }
            }
            result
        }

        assertEquals(movie!!.size, 20)
    }

    @Test
    fun readFromJsonFileAsStreamTest() {
        val movies: List<ItemMovieTv>? =
            ClassLoader.getSystemResourceAsStream(jsonFileName).use { inputStream ->
                Klaxon().fieldRenamer(renamer).parseArray(inputStream)
            }
        assertEquals(movies!!.size, 20)
    }
}
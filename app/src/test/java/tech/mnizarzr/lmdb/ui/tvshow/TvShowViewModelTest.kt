package tech.mnizarzr.lmdb.ui.tvshow

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(sdk = [29])
@RunWith(RobolectricTestRunner::class)
class TvShowViewModelTest {

    private lateinit var context: Context
    private lateinit var tvShowViewModel: TvShowViewModel

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        tvShowViewModel = TvShowViewModel()
    }

    @Test
    fun getTvShows() {
        val tvShows = tvShowViewModel.getTvShows(context)
        assertEquals(tvShows.size, 20)
    }
}
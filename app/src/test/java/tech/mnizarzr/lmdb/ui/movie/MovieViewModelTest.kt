package tech.mnizarzr.lmdb.ui.movie

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(sdk = [29])
@RunWith(RobolectricTestRunner::class)
class MovieViewModelTest {

    private lateinit var context: Context
    private lateinit var movieViewModel: MovieViewModel

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        movieViewModel = MovieViewModel()
    }

    @Test
    fun getMovies() {
        val movies = movieViewModel.getMovies(context)
        assertEquals(movies.size, 20)
    }
}
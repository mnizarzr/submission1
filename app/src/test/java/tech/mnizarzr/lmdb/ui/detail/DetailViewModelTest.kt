package tech.mnizarzr.lmdb.ui.detail

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import tech.mnizarzr.lmdb.entity.ItemMovieTv
import tech.mnizarzr.lmdb.utils.DataDummy

@Config(sdk = [29])
@RunWith(RobolectricTestRunner::class)
class DetailViewModelTest {

    private lateinit var detailViewModel: DetailViewModel
    private lateinit var context: Context
    private lateinit var item: ItemMovieTv
    private var itemId: Int? = null

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        detailViewModel = DetailViewModel()
        item = DataDummy.parseJsonAsset(context, DataDummy.FileName.Movies)[0]
        itemId = item.id
    }

    @Test
    fun getItem() {
        detailViewModel.setSelectedItem(item.id)
        val selectedItem = detailViewModel.getItem(context)
        assertEquals(item.id, selectedItem.id)
        assertEquals(item.backdropPath, selectedItem.backdropPath)
        assertEquals(item.overview, selectedItem.overview)
        assertEquals(item.posterPath, selectedItem.posterPath)
        assertEquals(item.releaseDate, selectedItem.releaseDate)
        assertEquals(item.title, selectedItem.title)
    }

}